/* See LICENSE file for copyright and license details.*/

#include <u.h>
#include <libc.h>
#include <thread.h>
#include <stdio.h> // perror
#include <9pclient.h>
#include <acme.h>


#include <sys/inotify.h>
#include <linux/limits.h>

Channel *done;

void
read_event(void *args) {
	int *fd = ((void **)args)[0];
	Channel *events = ((void **)args)[1];

	while (1) {
		struct inotify_event e;
		char buf[sizeof e * 4096] = {'\0'};

		int n = 0;
		if ((n = read(*fd, buf, sizeof buf)) <= 0) {
			fprint(2, "Error reading event watcher.....\n");
			perror("Watch");
			threadexitsall("fatal error");
		}

		int offset = 0;
		while (offset <= n - sizeof e) {
			struct inotify_event *raw = (struct inotify_event*)&buf[offset];
			e.mask = raw->mask;
			e.cookie = raw->cookie;
			e.len = raw->len;
			offset += sizeof e + e.len;
			send(events, &e);
		}
	}
}

void
discard_events(void *args) {
	Channel *done = ((void **)args)[0];
	Channel *events = ((void **)args)[1];
	enum {Done, Events, NKALT};
	Alt alts[NKALT + 1];
	int is_finish = 0;
	struct inotify_event ie;

	alts[Done].c = done;
	alts[Done].v = &is_finish;
	alts[Done].op = CHANRCV;

	alts[Events].c = events;
	alts[Events].v = &ie;
	alts[Events].op = CHANRCV;

	alts[NKALT].op = CHANEND;

	while (1) {
		switch(alt(alts)) {
		case Done:
			threadexits(nil);
			return;
			break;
		case Events:
			break;
		}
	}
}

Channel *
init_inotify(void) {
	static int fd;
	fd = inotify_init();
	if (fd < 0) {
		fprint(2, "Error adquiring inotify watcher file descriptor.\n");
		perror("Watch");
		return nil;
	}

	int wd = inotify_add_watch(fd, ".", IN_CREATE | IN_DELETE |
		IN_MODIFY | IN_MOVED_FROM | IN_MOVED_TO);
	if (wd < 0) {
		close(fd);
		fprint(2, "Error adquiring inotify watcher file descriptor.\n");
		perror("Watch");
		return nil;
	}

	Channel *events = chancreate(sizeof(struct inotify_event), 0);
	if(!events) return nil;

	static void *args[2];
	args[0] = &fd;
	args[1] = events;

	proccreate(read_event, args, 8192);

	return events;
}

void
cmd(void *arg) {
	Win *win = ((void **)arg)[0];
	char **argv = ((void **)arg)[1];

	char buffer[4096];
	buffer[0] = 0;

	int n = 0;
	while (*argv) {
		n = snprint(buffer + n, sizeof buffer - n, "%s ", *argv);
		argv++;
	}

	// clean the window
	winaddr(win, ",");
	winprint(win, "data", "");

	winprint(win, "body", "; %s\n", buffer);

	int stderr_fd = winfd(win, "body", OWRITE);
	int pid = pipetowin(win, "body", stderr_fd, buffer);

	waitfor(pid);

	winprint(win, "body", ";");
	winctl(win, "clean\n");
}


void
run_cmd(Win *win, Channel *ievents, char **argv) {

	proccreate(discard_events, (void *[]){done, ievents}, 4096);

	cmd((void *[]){win, argv});

	struct inotify_event e;
	nbrecv(ievents, &e); //recieve a posible last event.

	int finish = 1;
	send(done, &finish);
}

void
acme_events(void *args) {
	Channel *chan = ((void**)args)[0];
	Win *win = ((void **)args)[1];

	while(1) {
		struct Event *e;
		recv(chan, &e);

		switch(e->c2) {
		case 'x':
		case 'X':
			if (strcmp(e->text, "Del") == 0 || strcmp(e->text, "Delete") == 0) {
				windel(win, 1);
				threadexitsall(nil);
			}
		}
		winwriteevent(win, e);
	}
}

void
threadmain(int argc, char **argv) {
	if (argc < 2) {
		fprint(2, "Usage: Watch <command>\n");
		exit(1);
	}

	done = chancreate(sizeof(int), 1);

	Win *win = newwin();
	if(!win) threadexitsall("fatal");

	char cdir[4096];
	char *err = getwd(cdir, sizeof cdir);
	if(!err) {
		fprint(2, "Can't get current directory: %r\n");
		threadexitsall("fatal");
	}
	winname(win, "%s/+watch", cdir);


	threadcreate(acme_events, (void *[]){wineventchan(win), win}, 8192);


	Channel *ievents = init_inotify();
	if (!ievents) threadexitsall("fatal");

	run_cmd(win, ievents, argv + 1);

	while(1) {
		struct inotify_event e;
		recv(ievents, &e);
		run_cmd(win, ievents, argv + 1);
	}
}
